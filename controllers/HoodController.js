var Hood = require('../models/hood');

function getHoods(req,res){
    Hood.find({}).exec((err, hoods)=>{
        if(err) return res.status(401).json({error:"Some error occured. Please contact support if error persists"});
        return res.status(200).json({data:hoods} );
    });
}
function add(req, res){
    if(req.body.name){
        var hood = new Hood({name:req.body.name});
        hood.save((err, e)=>{
            if(err) return res.status(500).json({ error: 'Please contact support' });
            e.id = e._id
            e.save((err, finalE)=>{
                if(err) return res.status(500).json({ error: 'Please contact support' });
                return res.status(200).send(finalE);
            })
        })
    }
    else return res.status(400).send({error:"Bad Request"});
}
function remove(req, res){
    Hood.deleteOne({_id:req.params.id}).exec((err, hood)=>{
        if(err) return res.status(500).json({ error: 'Please contact support' });
        return res.status(200).send(hood);
    })
}
function getOne(req, res){
    Hood.findOne({_id:req.params.id}).exec((err, e)=>{
        if(err) return res.status(500).json({ error: 'Please contact support' });
        else return res.status(200).send(e);
    })
}
module.exports = {
    getHoods:getHoods,
    add: add,
    remove: remove,
    getOne: getOne
}