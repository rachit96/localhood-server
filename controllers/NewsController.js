var News = require('../models/news');
var _ = require('underscore');

function getNews(req,res){
    News.find({}).exec((err, news)=>{
        if(err) return res.status(401).json({error:"Some error occured. Please contact support if error persists"});
        return res.status(200).json({data:news} );
    });
}
function searchByName(req, res){
    News.find({"name" : {$regex : new RegExp(req.params.name,'i')}}).exec((err, news)=>{
        if(err) return res.status(401).json({ error: 'Please contact support' });
        return res.status(200).json({data:news});
    })
}
function edit(req, res){
    News.findOne({_id:req.params.id}).exec((err, news)=>{
        news = _.extend(news, req.body);
        console.log(news);
        news.save((err)=>{
            if(err) return res.status(401).json({ error: 'Please contact support' });
            return res.status(200).json({success:'saved'})
        })
    })
}
function remove(req, res){
    News.deleteOne({_id:req.params.id}).exec((err, news)=>{
        if(err) return res.status(401).json({ error: 'Please contact support' });
        return res.status(200).send(news);
    })
}
function add(req, res){
    // if(!req.body.name || !req.body.date || !req.body.day || !req.body.time || !req.body.details || !req.body.location || !req.body.image || !req.body.hood)
    //     return res.status(401).json({error:"missing param"});
    var news = new News({name:req.body.name, description:req.body.description, source:req.body.source, image:req.body.image, hood:req.body.hood});
    news.save((err, e)=>{
        if(err) return res.status(500).json({ error: 'Please contact support' });
        e.id = e._id
        e.save((err, finalE)=>{
            if(err) return res.status(500).json({ error: 'Please contact support' });
            return res.status(200).send(finalE);
        })
    }); 
}
function togglePublish(req, res){
    News.findOne({_id:req.params.id}).exec((err, news)=>{
        news.published = req.params.bool;
        console.log(news);
        news.save((err)=>{
            if(err) return res.status(401).json({ error: 'Please contact support' });
            return res.status(200).json({success:'saved'})
        })
    })
}
module.exports = {
    getNews: getNews,
    searchByName: searchByName,
    edit: edit,
    add: add,
    remove: remove,
    togglePublish: togglePublish
}