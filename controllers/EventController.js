var Event = require("../models/event");
var _ = require("underscore");
var csv = require("csv-express");
function getEvents(req, res) {
  Event.find({}).exec((err, events) => {
    if (err)
      return res
        .status(500)
        .json({
          error: "Some error occured. Please contact support if error persists"
        });
    return res.status(200).json({ data: events });
  });
}
function likeEvent(req, res) {
  Event.findOne({ _id: req.params.eventId }, function(err, event) {
    if (!event) return res.status(404).json({ error: "No such event" });
    if (event.likedBy.indexOf(req.params.userId) === -1)
      event.likedBy.push(req.params.userId);
    else return res.status(500).json({ error: "already liked the event" });
    event.save(function(err) {
      if (err) {
        return res.status(500).json({ error: "Please contact support" });
      }
      return res.json({ success: "event liked" });
    });
  });
}
function unlikeEvent(req, res) {
  Event.findOne({ _id: req.params.eventId }, function(err, event) {
    if (!event) return res.status(404).json({ error: "No such event" });
    var index = event.likedBy.indexOf(req.params.userId);
    if (index !== -1) event.likedBy.splice(index, 1);
    else return res.status(500).json({ error: "already unliked the event" });
    event.save(function(err) {
      if (err) {
        return res.status(500).json({ error: "Please contact support" });
      }
      return res.json({ success: "Event unliked" });
    });
  });
}
function going(req, res) {
  Event.findOne({ _id: req.params.eventId }, function(err, event) {
    if (!event) return res.status(404).json({ error: "No such event" });
    var index = event.going.indexOf(req.params.userId);
    if (event.going.indexOf(req.params.userId) === -1)
      event.going.push(req.params.userId);
    else return res.status(500).json({ error: "already going the event" });
    event.save(function(err) {
      if (err) {
        return res.status(500).json({ error: "Please contact support" });
      }
      return res.json({ success: "Event going" });
    });
  });
}
function notGoing(req, res) {
  Event.findOne({ _id: req.params.eventId }, function(err, event) {
    if (!event) return res.status(404).json({ error: "No such event" });
    var index = event.going.indexOf(req.params.userId);
    if (index !== -1) event.going.splice(index, 1);
    else
      return res.status(500).json({ error: "already not going to the event" });
    event.save(function(err) {
      if (err) {
        return res.status(500).json({ error: "Please contact support" });
      }
      return res.json({ success: "Event not going" });
    });
  });
}
function searchByName(req, res) {
  Event.find({ name: { $regex: new RegExp(req.params.name, "i") } }).exec(
    (err, events) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      return res.status(200).json({ data: events });
    }
  );
}

function filter(req, res) {
  var criteria = req.params.criteria;
  if (criteria == "hood") {
    Event.find({ hood: req.params.param }).exec((err, events) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      res.status(200).json({ data: events });
    });
  }
  // else if (criteria=="month"){
  //     var data=[]
  //     Event.find().exec((err, events)=>{
  //         if(err) return res.status(500).json({ error: 'Please contact support' });
  //         events.forEach(event => {
  //             date = event.date.split('/');
  //             month =  date[1];
  //             if(month == getMonthFromString(req.params.param)){
  //                 data.push(event);
  //             }
  //         });
  //         return res.status(200).json({data:data});
  //     })
  // }
  else {
    return res.status(200).json({ error: "Wrong Category" });
  }
}
function getMonthFromString(mon) {
  return new Date(Date.parse(mon + " 1, 2012")).getMonth() + 1;
}
function edit(req, res) {
  Event.findOne({ _id: req.params.id }).exec((err, event) => {
    if (err) return res.status(500).json({ error: err });
    else if (event) {
      event = _.extend(event, req.body);
      console.log(event);
      event.save(err => {
        if (err)
          return res.status(500).json({ error: "Please contact support" });
        return res.status(200).json({ success: "saved" });
      });
    }
    else return res.status(500).json({ error: "some error" });
  });
}
function remove(req, res) {
  Event.deleteOne({ _id: req.params.id }).exec((err, event) => {
    if (err) return res.status(500).json({ error: "Please contact support" });
    return res.status(200).send(event);
  });
}
function add(req, res) {
  var event = new Event({
    name: req.body.name,
    startTime: req.body.startTime,
    endTime: req.body.endTime,
    details: req.body.details,
    location: req.body.location,
    image: req.body.image,
    hood: req.body.hood
  });
  event.save((err, e) => {
    if (err) return res.status(500).json({ error: "Please contact support" });
    e.id = e._id;
    e.save((err, finalE) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      return res.status(200).send(finalE);
    });
  });
}
function exportGoing(req, res) {
  Event.findOne({ _id: req.params.id }, { name: 1, going: 1 })
    .populate("going")
    .exec((err, event) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      let tempEvents = [];
      event.going.forEach(element => {
        tempEvents.push(_.pick(element, ["email", "firstName", "lastName"]));
      });
      // r /es.statusCode = 200;
      // res.setHeader("Content-Type", "text/csv");
      // res.setHeader("Content-Disposition", "attachment; filename=xyz.csv");
      return res.status(200).send(tempEvents);
      // return res.status(200).send(tempEvents);
    });
}
function getOne(req, res) {
  Event.findOne({ _id: req.params.id }).exec((err, e) => {
    if (err) return res.status(500).json({ error: "Please contact support" });
    else return res.status(200).send(e);
  });
}
module.exports = {
  getEvents: getEvents,
  likeEvent: likeEvent,
  unlikeEvent: unlikeEvent,
  going: going,
  notGoing: notGoing,
  searchByName: searchByName,
  filter: filter,
  edit: edit,
  add: add,
  remove: remove,
  exportGoing: exportGoing,
  getOne: getOne
};
