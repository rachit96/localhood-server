var Deal = require("../models/deal");
var User = require("../models/user");
var _ = require("underscore");
var csv = require("csv-express");
function getDeals(req, res) {
  Deal.find({}).exec((err, deals) => {
    if (err)
      return res.status(500).json({
        error: "Some error occured. Please contact support if error persists"
      });
    res.status(200).json({ data: deals });
  });
}
function redeemDealCheckUserCode(req, res) {
  if (!req.params.userId || !req.params.userSecret)
    return res
      .status(400)
      .json({ error: "Please provide the appropriate parameters" });
  User.findOne({ _id: req.params.userId }).exec((err, user) => {
    if (err) return res.status(500).json({ error: "Please contact support" });
    if (user.secret == req.params.userSecret)
      return res.status(200).json({ success: "success" });
    else return res.status(404).json({ error: "wrong user secret" });
  });
}
function redeemDeal(req, res) {
  //merchant's code checking and redeeming
  if (!req.params.userId || !req.params.dealId || !req.params.merchantSecret)
    return res
      .status(400)
      .json({ error: "Please provide the appropriate parameters" });
  var userId = req.params.userId;
  var dealId = req.params.dealId;
  var merchantSecret = req.params.merchantSecret;

  Deal.findOne({ _id: dealId }, (err, deal) => {
    if (!deal) return res.status(404).json({ error: "No such deal" });
    if (deal.merchantSecret != merchantSecret)
      return res.status(500).json({
        error: "Wrong merchant code. Please try again or contact support!"
      });
    if (deal.redeemedBy.indexOf(userId) === -1) {
      deal.redeemedBy.push(userId);
    } else return res.status(500).json({ error: "already redeemed" });
    deal.save(err => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      res.json({ success: "Deal Redeemed" });
    });
  });
}
function likeDeal(req, res) {
  Deal.findOne({ _id: req.params.dealId }, function(err, deal) {
    if (!deal) return res.status(404).json({ error: "No such deal" });
    if (deal.likedBy.indexOf(req.params.userId) === -1)
      deal.likedBy.push(req.params.userId);
    else return res.status(500).json({ error: "already liked the deal" });
    deal.save(function(err) {
      if (err) {
        return res.status(500).json({ error: "Please contact support" });
      }
      res.json({ success: "deal liked" });
    });
  });
}
function unlikeDeal(req, res) {
  Deal.findOne({ _id: req.params.dealId }, function(err, deal) {
    if (!deal) return res.status(404).json({ error: "No such deal" });
    var index = deal.likedBy.indexOf(req.params.userId);
    if (index !== -1) deal.likedBy.splice(index, 1);
    else return res.status(500).json({ error: "already unliked the deal" });
    deal.save(function(err) {
      if (err) {
        return res.status(500).json({ error: "Please contact support" });
      }
      res.json({ success: "Deal unliked" });
    });
  });
}
function searchByName(req, res) {
  Deal.find({ name: { $regex: new RegExp(req.params.name, "i") } }).exec(
    (err, deals) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      res.status(200).json({ data: deals });
    }
  );
}

function filter(req, res) {
  var criteria = req.params.criteria;
  if (criteria == "hood") {
    Deal.find({ hood: req.params.param }).exec((err, deals) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      res.status(200).json({ data: deals });
    });
  } else if (criteria == "category") {
    Deal.find({ category: req.params.param }).exec((err, deals) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      res.status(200).json({ data: deals });
    });
  }
  // else if (criteria=="month"){
  //     var data=[]
  //     Deal.find().exec((err, deals)=>{
  //         if(err) return res.status(500).json({ error: 'Please contact support' });
  //         deals.forEach(deal => {
  //             date = deal.validTill.split('/');
  //             month =  date[1];
  //             if(month == getMonthFromString(req.params.param)){
  //                 data.push(deal);
  //             }
  //         });
  //         res.status(200).json({data:data});
  //     })
  // }
  else {
    res.status(200).json({ error: "Wrong Category" });
  }
}
function getMonthFromString(mon) {
  return new Date(Date.parse(mon + " 1, 2012")).getMonth() + 1;
}
function edit(req, res) {
  Deal.findOne({ _id: req.params.id }).exec((err, deal) => {
    console.log(err);
    if (err) return res.status(500).json({ error: err });
    deal = _.extend(deal, req.body);
    console.log(deal);
    deal.save(err => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      return res.status(200).send("ok");
    });
  });
}
function remove(req, res) {
  Deal.deleteOne({ _id: req.params.id }).exec((err, deal) => {
    if (err) return res.status(500).json({ error: "Please contact support" });
    return res.status(200).send(deal);
  });
}
function add(req, res) {
  // if(!req.body.name || !req.body.date || !req.body.day || !req.body.time || !req.body.details || !req.body.location || !req.body.image || !req.body.hood)
  //     return res.status(500).json({error:"missing param"});
  var deal = new Deal({
    name: req.body.name,
    subDetails: req.body.subDetails,
    image: req.body.image,
    validTill: req.body.validTill,
    details: req.body.details,
    location: req.body.location,
    deal: req.body.deal,
    hood: req.body.hood,
    merchantSecret: req.body.merchantSecret,
    category: req.body.category
  });
  deal.save((err, e) => {
    if (err) return res.status(500).json({ error: err });
    e.id = e._id;
    e.save((err, finalE) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      return res.status(200).send(finalE);
    });
  });
}
function exportRedeemedBy(req, res) {
  Deal.findOne({ _id: req.params.id }, { name: 1, redeemedBy: 1 })
    .populate("redeemedBy")
    .exec((err, deal) => {
      if (err) return res.status(500).json({ error: "Please contact support" });
      let tempDeals = [];
      deal.redeemedBy.forEach(element => {
        tempDeals.push(_.pick(element, ["firstName", "lastName", "email"]));
      });
      // res.statusCode = 200;
      // res.setHeader("Content-Type", "text/csv");
      // res.setHeader("Content-Disposition", "attachment; filename=" + 'xyz');
      return res.status(200).send(tempDeals);
    });
}
function getOne(req, res) {
  Deal.findOne({ _id: req.params.id }).exec((err, e) => {
    if (err) return res.status(500).json({ error: "Please contact support" });
    else return res.status(200).send(e);
  });
}
module.exports = {
  getDeals: getDeals,
  redeemDeal: redeemDeal,
  likeDeal: likeDeal,
  unlikeDeal: unlikeDeal,
  searchByName: searchByName,
  filter: filter,
  redeemDealCheckUserCode: redeemDealCheckUserCode,
  edit: edit,
  add: add,
  remove: remove,
  exportRedeemedBy: exportRedeemedBy,
  getOne: getOne
};

// db.deals.findOne({"name" : {$regex : "son.*"}});
