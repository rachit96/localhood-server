var User = require("../models/user");
var passport = require("passport");
var jwt = require("jsonwebtoken");
var async = require("async");
var crypto = require("crypto");
var nodemailer = require("nodemailer");
var xoauth2 = require("xoauth2");
var csv = require("csv-express");

require('dotenv').config();
function register(req, res) {
  if (
    !req.body.username ||
    !req.body.password ||
    !req.body.confirmPassword ||
    !req.body.firstName ||
    !req.body.lastName ||
    !req.body.securityAnswer ||
    !req.body.securityQuestion ||
    !req.body.secret ||
    !req.body.phone
  )
    return res.status(401).json({ error: "missing params" });
  if (req.body.confirmPassword !== req.body.password)
    return res.status(401).json({ error: "passwords dont match" });
  if (req.body.type) {
    User.register(
      new User({
        email: req.body.username,
        phone: req.body.phone,
        securityQuestion: req.body.securityQuestion,
        securityAnswer: req.body.securityAnswer,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        secret: req.body.secret,
        type: "admin"
      }),
      req.body.password,
      function(err, user) {
        if (err) {
          return res.status(400).send({ error: "Email address in use." });
        }
        res.status(200).send({ user: user.id });
      }
    );
  } else {
    User.register(
      new User({
        email: req.body.username,
        phone: req.body.phone,
        securityQuestion: req.body.securityQuestion,
        securityAnswer: req.body.securityAnswer,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        secret: req.body.secret
      }),
      req.body.password,
      function(err, user) {
        if (err) {
          return res.status(400).send({ error: "Email address in use." });
        }
        user.id = user._id;
        user.save(err => {
          if (err)
            return res.status(500).json({ error: "Please contact support" });
          return res.status(200).send("ok");
        });
      }
    );
  }
}
function login(req, res, next) {
  passport.authenticate("local", function(err, user, info) {
    if (err) {
      return next(err);
    }
    console.log(info);
    if (!user) {
      return res.status(401).json({ error: "Invalid credentials." });
    }
    if (user) {
      if (user.isFirstTime) {
        var token = jwt.sign(
          { id: user._id, email: user.email },
          "7x0jhxt&quot;9(thpX6",
          { expiresIn: "7 days" }
        );
        return res.status(200).json({ token, isFirstTime: true });
      } else {
        var token = jwt.sign(
          { id: user._id, email: user.email },
          "7x0jhxt&quot;9(thpX6",
          { expiresIn: "7 days" }
        );
        return res.status(200).json({ token, isFirstTime: false });
      }
    }
  })(req, res, next);
}
function chooseYourHood(req, res) {
  if (!req.body.hood) return res.status(400).json({ error: "Bad Request" });
  if (req.headers && req.headers.authorization) {
    var jwtoken = req.headers.authorization.split(" ")[1];
    var userId = jwt.decode(jwtoken, "7x0jhxt&quot;9(thpX6").id;
  } else {
    return res.status(401).json({ error: "Invalid credentials." });
  }
  User.findOne({ _id: userId }, (err, user) => {
    if (err) return res.status(401).json({ error: "Invalid credentials." });
    user.hood = req.body.hood;
    user.save(err => {
      if (err)
        return res.status(401).json({ error: "Some error. Please try again" });
      return res.status(200).send("ok");
    });
  });
}
function forgotPassword(req, res, next) {
  if (!req.body.email) return res.status(401).json({ error: "missing params" });
  async.waterfall(
    [
      function(done) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString("hex");
          done(err, token);
        });
      },
      function(token, done) {
        User.findOne({ email: req.body.email }, function(err, user) {
          if (err) res.status(500).json({ error: "Some error occured" });
          if (!user)
            return res.status(401).json({ error: "No user with this email" });
          user.resetPasswordToken = token;
          user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
          user.save(function(err) {
            done(err, token, user);
          });
        });
      },
      function(token, user, done) {
        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "test@appit.com.hk",
            pass: "testing@appit"
          }
        });
        const mailOptions = {
          from: "test@appit.com.hk", // sender address
          to: user.email, // list of receivers
          subject: "Localhood Password Reset",
          text:
            "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
            "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
            "http://" +
            process.env.FRONTEND_URL +
            "/reset?token=" +
            token +
            "\n\n" +
            "If you did not request this, please ignore this email and your password will remain unchanged.\n"
        };
        transporter.sendMail(mailOptions, function(err, info) {
          if (err) return res.status(500).json({ error: err });
          return res.status(200).json({ success: "email sent" });
          done(err, "done");
        });
      }
    ],
    function(err) {
      if (err) return next(err);
    }
  );
}
function tokenValidator(req, res) {
  User.findOne(
    {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: { $gt: Date.now() }
    },
    function(err, user) {
      if (!user) {
        return res.status(401).json({ error: "Invalid or expired token" });
      }
      return res.status(200).json({ success: "valid token" });
    }
  );
}
function resetPassword(req, res) {
  if (req.body.confirmPassword !== req.body.password)
    return res.status(401).json({ error: "passwords dont match" });
  User.findOne(
    {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: { $gt: Date.now() }
    },
    function(err, user) {
      if (!user) {
        return res.status(401).json({ error: "Invalid or expired token" });
      }
      user.setPassword(req.body.password, () => {
        user.save(err => {
          if (err)
            return res
              .status(401)
              .json({ error: "Some error. Please try again" });
          // user.resetPasswordToken = undefined;
          // user.resetPasswordExpires = undefined;
          return res.status(200).json({ success: "password changed" });
        });
      });
    }
  );
  // return res.status(401).json({error:"Some error. Please try again"});
}
function exportMembers(req, res) {
  User.find(
    { type: { $ne: "admin" } },
    { firstName: 1, lastName: 1, hood: 1, phone: 1, email: 1 }
  ).populate('hood').lean().exec((err, users) => {
    let tempUsers = [];
    users.forEach(element => {
      element.hood = element.hood.name;
      tempUsers.push(element)
      
    });
  
    return res.status(200).send(tempUsers);
    // return res.status(200).send(tempUsers);
  });
}
function search(req, res) {
  if (req.params.criteria == "first") {
    User.find({
      firstName: { $regex: new RegExp(req.params.value, "i") }
    }).exec((err, news) => {
      if (err) return res.status(401).json({ error: "Please contact support" });
      return res.status(200).json({ data: news });
    });
  } else if (req.params.criteria == "last") {
    User.find({ lastName: { $regex: new RegExp(req.params.value, "i") } }).exec(
      (err, news) => {
        if (err)
          return res.status(401).json({ error: "Please contact support" });
        return res.status(200).json({ data: news });
      }
    );
  } else if (req.params.criteria == "phone") {
    User.find({ phone: { $regex: new RegExp(req.params.value, "i") } }).exec(
      (err, news) => {
        if (err)
          return res.status(401).json({ error: "Please contact support" });
        return res.status(200).json({ data: news });
      }
    );
  } else if (req.params.criteria == "email") {
    User.find({ email: { $regex: new RegExp(req.params.value, "i") } }).exec(
      (err, news) => {
        if (err)
          return res.status(401).json({ error: "Please contact support" });
        return res.status(200).json({ data: news });
      }
    );
  }
}
function getUserInfo(req, res) {
  if (req.headers && req.headers.authorization) {
    let token = req.headers.authorization.split(" ")[1];
    let userId = jwt.decode(token, "7x0jhxt&quot;9(thpX6").id;

    User.findOne(
      { _id: userId },
      { firstName: 1, lastName: 1, phone: 1, email: 1 }
    ).exec((err, user) => {
      console.log(user);
      if (err) return res.status(500).send({ error: "Internal Server Error" });
      else if (!user) res.status(404).send({ error: "User not found" });
      else if (user) return res.status(200).send({ data: user });
      else return res.status(400).send({ error: "Bad Request" });
    });
  } else return res.status(400).send({ error: "Bad Request" });
}
function editUserInfo(req, res) {
  if (req.headers && req.headers.authorization) {
    let token = req.headers.authorization.split(" ")[1];
    let userId = jwt.decode(token, "7x0jhxt&quot;9(thpX6").id;

    User.findOne(
      { _id: userId }
    ).exec((err, user) => {
      if (err) return res.status(401).json({ error: "Some error. Please try again" });
      user = _.extend(user, req.body);
      user.save(err => {
        if (err)
          return res.status(401).json({ error: "Some error. Please try again" });
        return res.status(200).send("ok");
      });
    });
  } else return res.status(400).send({ error: "Bad Request" });
}
function getAllUsers(req, res){
  User.find().exec((err, users)=>{
    if(err) return res.status(401).json({ error: "Some error. Please try again" });
    return res.status(200).send({data:users})
  })
}
module.exports = {
  register: register,
  login: login,
  chooseYourHood: chooseYourHood,
  forgotPassword: forgotPassword,
  resetPassword: resetPassword,
  tokenValidator: tokenValidator,
  exportMembers: exportMembers,
  search: search,
  getUserInfo: getUserInfo,
  editUserInfo, 
  getAllUsers
};
