var express = require("express");
var passport = require("passport");
var jwt = require("jsonwebtoken");

function authMiddleware(req, res, next) {
  passport.authenticate("jwt", function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(401).json({ error: "Invalid credentials." });
    }
    if (user) {
      // authentication was successful! send user the secret code.
      next();
    }
  })(req, res, next);
}
module.exports={
    authMiddleware:authMiddleware
}
