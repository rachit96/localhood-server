var express = require("express");
var router = express.Router();
var passport = require("passport");
var jwt = require("jsonwebtoken");
// var authMiddleware = require('../middleware/authMiddleware');

const eventController = require("../controllers/EventController"),
  dealController = require("../controllers/DealController"),
  userController = require("../controllers/UserController"),
  newsController = require("../controllers/NewsController"),
  hoodController = require('../controllers/HoodController');
//todo: move to appropritae folder
function authMiddleware(req, res, next) {
   
    
    // console.log(jwt)
  passport.authenticate("jwt", function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(401).json({ error: "Invalid credentials." });
    }
    if (user) {
      // authentication was successful! send user the secret code.
      next();
    }
  })(req, res, next);
}
//Events routes
router.get("/events", eventController.getEvents);
router.get("/deals", dealController.getDeals);
router.get('/hoods', hoodController.getHoods);

router.post("/deals/redeem/:userId/:dealId/:merchantSecret", authMiddleware, dealController.redeemDeal);
router.post("/deals/like/:userId/:dealId", authMiddleware, dealController.likeDeal);
router.post("/deals/unlike/:userId/:dealId", authMiddleware, dealController.unlikeDeal);
router.get("/deals/searchbyname/:name", dealController.searchByName);
router.get("/deals/filter/:criteria/:param", dealController.filter);
router.get('/deals/redeem/checkusersecret/:userId/:userSecret', authMiddleware, dealController.redeemDealCheckUserCode);


router.post("/events/like/:userId/:eventId", authMiddleware, eventController.likeEvent);
router.post("/events/unlike/:userId/:eventId", authMiddleware, eventController.unlikeEvent);
router.post("/events/going/:userId/:eventId", authMiddleware, eventController.going);
router.post("/events/notGoing/:userId/:eventId", authMiddleware, eventController.notGoing);
router.get("/events/searchbyname/:name", eventController.searchByName);

router.get("/events/filter/:criteria/:param", eventController.filter);

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post('/chooseYourHood', authMiddleware, userController.chooseYourHood);

router.get('/user/info', authMiddleware, userController.getUserInfo)
//get all hoods
router.put('/user/info', authMiddleware, userController.editUserInfo);
router.get("/logout", (req, res) => {

});

router.post("/forgot", userController.forgotPassword);
router.get('/reset/:token', userController.tokenValidator);
router.post('/reset/:token', userController.resetPassword);
router.get("/news", newsController.getNews);
router.get("/news/searchbyname/:name", newsController.searchByName);
module.exports = router;
