var express = require("express");
var router = express.Router();
var passport = require("passport");
var jwt = require("jsonwebtoken");

const eventController = require("../controllers/EventController"),
  dealController = require("../controllers/DealController"),
  userController = require("../controllers/UserController"),
  newsController = require("../controllers/NewsController"),
  hoodController = require("../controllers/HoodController"),
  uploadController = require('../controllers/UploadController')
  ;
//todo: move to appropritae folder
function authMiddleware(req, res, next) {
  passport.authenticate("jwt", function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(401).json({ error: "Invalid credentials." });
    }
    if (user) {
        if(user.type==='admin') next();
        else return res.status(401).json({error:"access denied"});
    }
  })(req, res, next);
}
router.post("/login", userController.login);


router.get('/events/', authMiddleware, eventController.getEvents);
router.post('/events', eventController.add);
router.put('/events/:id', authMiddleware, eventController.edit);
router.delete('/events/:id', authMiddleware, eventController.remove);

router.get('/deals/', authMiddleware, dealController.getDeals);
router.post('/deals', dealController.add);
router.put('/deals/:id', authMiddleware, dealController.edit);
router.delete('/deals/:id', authMiddleware, dealController.remove);


router.get('/hoods/', authMiddleware, hoodController.getHoods);
router.post('/hoods', authMiddleware, hoodController.add);
router.delete('/hoods/:id', authMiddleware, hoodController.remove);

router.get("/news", newsController.getNews);
router.post('/news', authMiddleware, newsController.add);
router.put('/news/:id', authMiddleware, newsController.edit);
router.delete('/news/:id', authMiddleware, newsController.remove);
router.post('/news/publish/:id/:bool', authMiddleware, newsController.togglePublish);

router.get('/export/members', authMiddleware, userController.exportMembers);
router.get('/export/event/:id/going', authMiddleware, eventController.exportGoing);
router.get('/export/deal/:id/redeemedby', authMiddleware, dealController.exportRedeemedBy);
router.get('/users/all', authMiddleware, userController.getAllUsers)

router.get('/search/:criteria/:value', authMiddleware, userController.search);

router.get('/hoods/:id', authMiddleware, hoodController.getOne);
router.get('/events/:id', authMiddleware, eventController.getOne);
router.get('/deals/:id', authMiddleware, dealController.getOne);

router.post('/events/uploads', uploadController.upload)
router.post('/deals/uploads', uploadController.upload)
router.post('/hoods/uploads', uploadController.upload)
router.post('/news/uploads', uploadController.upload)
// router.post('/events/uploads', uploadController.upload)

module.exports = router;
