var mongoose = require("mongoose");

const eventSchema = new mongoose.Schema({
  name: {
    type: String
  },
  startTime: {
    type: Date
  },
  endTime: {
    type: Date
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  likedBy: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  ],
  comments: [
    {
      author: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User"
      },
      text: { type: String, required: true },
      createdAt: { type: Date, default: Date.now }
    }
  ],
  details: {
    type: String
  },
  location:{
      type:String
  },
  going:[
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  ],
  image:{
    type:String
  },
  hood:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Hood"
  },
  id:{
    type:String
  }
});

// db.events.insert({'name':'Sample Event', date:'12/12/2018', day:'Sat', time:'7:00PM', details:'Lorem Ipsum', location:'13-D Hollywood Center, 77-91 Queen\'s Road West, Sheung Wan, Hong Kong' });
// db.items.insert({ 'name':'Pineapple', inspector:"Best", calories:190, image:"random", marketType:"fruits"});
module.exports = mongoose.model("Event", eventSchema);
