var mongoose = require("mongoose");
const newsSchema = new mongoose.Schema({
    name:String,
    description:String,
    source:String,
    image:String,
    hood:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Hood"
    },
    published:{
        type:Boolean,
        default:true
    },
    id:String
});

// db.deals.insert({'name':'La Creperie', subDetails:'Indulge with delicious and galettes!', validTill:'12/12/2018', details:'Lorem Ipsum', location:'13-D Hollywood Center, 77-91 Queen\'s Road West, Sheung Wan, Hong Kong' });
// db.items.insert({ 'name':'Pineapple', inspector:"Best", calories:190, image:"random", marketType:"fruits"});
module.exports = mongoose.model("News", newsSchema);
