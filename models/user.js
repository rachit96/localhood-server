var mongoose = require("mongoose");

var passportLocalMongoose = require("passport-local-mongoose");

var userSchema = mongoose.Schema;
var User = new userSchema({
  hood:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Hood"
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now
  },
  phone:{
    type:String
  },
  securityQuestion:{
    type:String
  },
  securityAnswer:{
    type:String
  },
  firstName:String,
  lastName:String,
  secret:Number,
  resetPasswordToken:String,
  resetPasswordExpires:Date,
  type:{
    type:String,
    default:""
  },
  isFirstTime:{
    type:Boolean,
    default:true
  },
  id:String

});
User.plugin(passportLocalMongoose, {
  usernameField: "email",
  usernameUnique: true
});
module.exports = mongoose.model("User", User);
