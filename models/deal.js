var mongoose = require("mongoose");

const dealSchema = new mongoose.Schema({
  name: {
    type: String
  },
  subDetails:{
    type:String
  },
  image: {
    type:String
  },
  validTill: {
    type: Date
  },
  deal: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  likedBy: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  ],
  details: {
    type: String
  },
  location:{
      type:String
  },
  redeemedBy:[
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  ],
  merchantSecret:{
      type:Number
  },
  hood:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Hood"
  },
  category:{
    type:String
  },
  id:String
});

// db.deals.insert({'name':'La Creperie', subDetails:'Indulge with delicious and galettes!', validTill:'12/12/2018', details:'Lorem Ipsum', location:'13-D Hollywood Center, 77-91 Queen\'s Road West, Sheung Wan, Hong Kong' });
// db.items.insert({ 'name':'Pineapple', inspector:"Best", calories:190, image:"random", marketType:"fruits"});
module.exports = mongoose.model("Deal", dealSchema);
