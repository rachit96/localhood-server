var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var mongoose = require("mongoose");
var cors = require("cors");
var indexRouter = require("./routes/index");
var protectedRouter = require('./routes/protected');
var apiRouter = require('./routes/api')
const bodyParser = require("body-parser");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var User = require('./models/user')
require('dotenv').config();

//mongo db setup
var mongoDB = "mongodb://" + process.env.DB_HOST + "/" + process.env.DB_NAME;
console.log(mongoDB)
mongoose.connect(mongoDB); // Get Mongoose to use the global promise library
mongoose.Promise = global.Promise; //Get the default connection
var db = mongoose.connection; //Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind(console, "MongoDB connection error:"));


passport.use('local', new LocalStrategy(User.authenticate()));

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // to support JSON-encoded bodies

app.use(logger("dev"));
app.use(cors({ origin: "*" }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

var options = {}
options.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
options.secretOrKey = '7x0jhxt&quot;9(thpX6'
passport.use('jwt', new JwtStrategy(options, function(jwt_payload, done) {
  User.findOne({
    _id: jwt_payload.id
  }, function(err, user) {
    if (err) {
      return done(err, false);
    }
    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  })
}))
app.use("/v1/", indexRouter);
app.use('/api', apiRouter);

app.use(function(req, res, next) {
  next(createError(404));
});
module.exports = app;